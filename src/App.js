import React, { useState } from 'react';
import logo from './logo.svg';
import NameCard from './components/NameCard'
import LikeButton from './components/LikeButton'
import Clock from './components/Clock'
import CommentBox from './components/CommentBox'
import ThemeBar from './components/ThemeBar'
import Comment from './comment/Comment'
import ThemeContext from './theme-context'

import './App.css';

function App() {
  // 函数组件使用useState来初始化变量以及更改变量
  const [theme, setTheme] = useState('light')

  const tags = [
    '菜鸡',
    '超级好看'
  ]
  const themes = {
    light: {
      classnames: 'btn btn-primary',
      bgColor: '#eee',
      color: '#000'
    },
    dark: {
      classnames: 'btn btn-light',
      bgColor: '#222',
      color: '#fff'
    }
  }

  const changeTheme = (theme) => {
    setTheme(theme)
  }

  return (
    // value为context传入的公用数据，在Provider包裹内都可以使用该数据
    <ThemeContext.Provider value={themes[theme]}>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
        </header>

        {/* <NameCard tags={tags} name='牛牛' number={1322222222} isHuman/> */}
        {/* <LikeButton /> */}
        {/* <Clock /> */}
        {/* <CommentBox /> */}
        {/* <Comment /> */}

        <a
        href='#theme-switcher'
          onClick={() => changeTheme('light')}
        className='btn btn-light'>
          浅色主题
        </a>
        <a
        href='#theme-switcher'
          onClick={() => changeTheme('dark')}
        className='btn btn-secondary'>
          深色主题
        </a>
        <ThemeBar></ThemeBar>
      </div>
    </ThemeContext.Provider>
  );
}

export default App;
