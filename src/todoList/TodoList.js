import React, {useState, useCallback, useRef, useEffect, memo} from 'react'
import './TodoList.css'
import {
    createSet,
    createAdd,
    createRemove,
    createToggle
} from './action'
import reducer from './reducers';

// 三、使用action.js再次封装
function bindActionCreators(actionCreators, dispatch) {
    const ret = {}
    for (let key in actionCreators) {
        ret[key] = function (...args) {
            const actionCreator = actionCreators[key]
            console.log(actionCreator);
            
            const action = actionCreator(...args)
            console.log(action);
            dispatch(action)
        }
    }
    return ret
}

// let idSeq = Date.now()
let LS_KEY = '_$-todos_'
// useCallback用于优化
const Control = memo(function Control (props) {
    // const {addTodo} = props
    // const {dispatch} = props
    const { addTodoNew } = props
    const inputRef = useRef()
    const onSubmit = (e) => {
        e.preventDefault();
        const newText = inputRef.current.value.trim()
        if (!newText) {
            return
        }
        // addTodo({
        //     id: ++idSeq,
        //     text: newText,
        //     complete: false
        // }) // 一
        // addTodoNew({
        //     id: ++idSeq,
        //     text: newText,
        //     complete: false
        // }) // 三
        addTodoNew(newText) // 四
        // dispatch({
        //     type: 'add',
        //     payload: {
        //         id: ++idSeq,
        //         text: newText,
        //         complete: false
        //     }
        // }) 二

        // dispatch(createAdd({
        //     id: ++idSeq,
        //     text: newText,
        //     complete: false
        // })) 三

        inputRef.current.value = ''
    }
    return (
        <div className='control'>
            <h1>todos</h1>
            <form onSubmit={onSubmit}>
                <input
                    ref={inputRef}
                    className='new-todo'
                    placeholder='请输入待办'
                />
            </form>
        </div>
    )
})

const Todos = memo(function Todos (props) {
    // const { todos, removeTodo, toggleTodo} = props
    // const { todos, dispatch} = props
    const { todos, removeTodoNew, toggleTodoNew} = props
    return (
        <div>
            <ul className='todos'>
                {
                    todos.map(todo => {
                        return (
                            <TodoItem 
                                key={todo.id}
                                todo={todo}
                                // toggleTodo={toggleTodo}
                                // removeTodo={removeTodo}
                                // dispatch={dispatch}
                                toggleTodoNew={toggleTodoNew}
                                removeTodoNew={removeTodoNew}
                            />
                        )
                    })
                }
            </ul>
        </div>
    )
})

const TodoItem = memo(function TodoItem(props) {
    const {
        todo: {
            id,
            text,
            complete
        },
        // toggleTodo,
        // removeTodo
        // dispatch
        toggleTodoNew,
        removeTodoNew
    } = props

    const onChange = () => {
        // toggleTodo(id) // 一
        toggleTodoNew(id) // 三
        // dispatch({ type: 'toggle', payload: id }) // 二
        // dispatch(createToggle(id))
    }

    const onRemove = () => {
        // removeTodo(id) // 一 
        removeTodoNew(id) //  三
        // dispatch({type: 'remove', payload: id}) 二
        // dispatch(createRemove(id))
    }

    return (
        <li className='todo-item'>
            <input
                type='checkbox'
                onChange={onChange}
                checked={complete}
            />
            <label className={
                complete ? 'complete' : ''
            }>{text}</label>
            <button onClick={onRemove}>删除</button>
        </li>
    )
})

let store = {
    todos: [],
    incrementCount: 0
}

function TodoList() {
    const [todos, setTodos] = useState([])
    const [incrementCount, setIncrementCount] = useState(0)

    useEffect(() => {
        Object.assign(store, {
            todos,
            incrementCount
        })
    }, [todos, incrementCount])

    // 一、1.添加
    const addTodo = useCallback((todo) => {
        setTodos(todos => [...todos, todo])
    }, [])

    // 一、2.删除
    const removeTodo = useCallback((id) => {
        setTodos(todos => todos.filter(todo => {
            return todo.id !== id
        }))
    },[])

    // 一、3.切换
    const toggleTodo = useCallback((id) => {
        setTodos(todos => todos.map(todo => {
            return todo.id === id
            ? {
                ...todo,
                complete: !todo.complete
            }
            : todo
        }))
    },[])

    // 四-封装在reducers.js

    const dispatch = (action) => {

        const setters = {
            todos: setTodos,
            incrementCount: setIncrementCount
        }

        // 如果是函数，就传入参数并执行
        if ('function' === typeof action) {
            action(dispatch, () => store)
            return
        }

        const newState = reducer(store, action)

        for (let key in newState) {
            setters[key](newState[key])
        }

        // action.reduce(function (lastTodos, actions) {
        //     return [...lastTodos, action.pal]
        // }, todo)

    }

    // 二、使用具有语义的函数来执行副作用的操作
    // const dispatch = useCallback((action) => {
    //     const {type, payload} = action

    //     switch (type) {
    //         case 'set':
    //             setTodos(payload)
    //             setIncrementCount(c => c + 1)
    //             break
    //         case 'add':
    //             setTodos(todos => [...todos, payload])
    //             setIncrementCount(c => c + 1)
    //             break
    //         case 'remove':
    //             setTodos(todos => todos.filter(todo => {
    //                 return todo.id !== payload
    //             }))
    //             break
    //         case 'toggle':
    //             setTodos(todos => todos.map(todo => {
    //                 return todo.id === payload
    //                     ? {
    //                         ...todo,
    //                         complete: !todo.complete
    //                     }
    //                     : todo
    //             }))
    //             break
    //         default:
    //     }
    // }, [])

    // 两个副作用顺序必须如下：先读再写
    useEffect(() => {
        const todos = JSON.parse(localStorage.getItem(LS_KEY) || '[]')
        // setTodos(todos) 一
        // dispatch({type: 'set', payload: todos}) 二
        dispatch(createSet(todos))
    }, [])

    useEffect(() => {
        localStorage.setItem(LS_KEY, JSON.stringify(todos))
    }, [todos])

    return (
        <div className='todo-list'>
            {/* <Control addTodo={addTodo}/> */}
            {/* <Todos removeTodo={removeTodo} toggleTodo={toggleTodo} todos={todos}/> */}

            {/* <Control dispatch={dispatch}/> */}
            {/* <Todos dispatch={dispatch} todos={todos}/> */}

            <Control {
                ...bindActionCreators({
                    addTodoNew: createAdd
                }, dispatch)
            } />
            <Todos {
                ...bindActionCreators({
                    removeTodoNew: createRemove,
                    toggleTodoNew: createToggle
                }, dispatch)
            } todos={todos} />
        </div>
    )
}

export default TodoList