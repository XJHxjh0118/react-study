// 从数据构造的维度处理action，如此不同字段之间就不会有过多的干扰
const reducers = {
    todos(state, action) {
        const { type, payload } = action

        switch (type) {
            case 'set':
                return payload
            case 'add':
                return [...state, payload]
            case 'remove':
                return state.filter(todo => {
                    return todo.id !== payload
                })
            case 'toggle':
                return state.map(todo => {
                    return todo.id === payload ? {
                        ...todo,
                        complete: !todo.complete
                    } : todo
                })
        }

        return state
    },
    incrementCount(state, action) {
        const { type, payload } = action

        switch (type) {
            case 'set':
                return state + 1
            case 'add':
                return state + 1
        }

        return state
    }
}

// 四、使用reduce函数
function combineReducers(reducers) {
    return function reducer(state, action) {
        const changed = {}
        for (let key in reducers) {
            changed[key] = reducers[key](state[key], action)
        }

        return {
            ...state,
            ...changed
        }
    }
}

export default combineReducers(reducers)