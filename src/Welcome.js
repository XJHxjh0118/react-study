import React from 'react'

class Welcome extends React.Component {
    constructor () {
        super()
        this.state = {
            isLogin: false
        }
    }
    loginIn () {
        this.setState({
            isLogin: !this.state.isLogin
        })
    }

    render() {
        const todoList = [
            'learn React',
            'learn Redux'
        ]
        const loginTrue = <div>已经登录</div>
        const loginFalse = <div>未登录</div>
        return (
            <div>
                <h1> helloworld </h1>
                {
                    this.state.isLogin ? loginTrue : loginFalse
                }
                <button onClick={this.loginIn.bind(this)}>
                    {this.state.isLogin ? '注销' : '登录'}
                </button>
            </div>
        )
    }
}

export default Welcome