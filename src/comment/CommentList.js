import React from 'react'

class CommentList extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {list} = this.props
        return (
            <div>
                <h4>评论列表</h4>
                <ul className='list-group mb-3'>
                    {
                        list.map((item, index) => (
                            <li key={index} className='list-group-item'>{item}</li>
                        ))
                    }
                </ul>
            </div>
        )
    }
}

export default CommentList
