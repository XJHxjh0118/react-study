import React from 'react'

class CommentBox extends React.Component {
    constructor(props) {
        super(props)
    }

    submitComment (e) {
        e.preventDefault()
        // 触发父级组件传入的jsx属性
        this.props.onAddComment(this.textValue.value)
    }

    render() {
        const { list } = this.props
        return (
            <div>
                <h4>留下评论</h4>
                <form onSubmit={this.submitComment.bind(this)}>
                    <input 
                        ref={(textValue) => {
                            this.textValue = textValue
                        }}
                    />

                    <button>
                        发表
                    </button>
                    <span>已有{list.length}条评论</span>
                </form>
            </div>
        )
    }
}

export default CommentBox
