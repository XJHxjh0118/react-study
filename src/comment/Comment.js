import React from 'react'
import CommentBox from './CommentBox'
import CommentList from './CommentList'

class Comment extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            list: ['很好', '满意']
        }
    }

    addComment (comment) {
        this.setState({
            list: [...this.state.list, comment]
        })
    }

    render() {
        const {list} = this.state
        return (
            <div>
                <CommentList list={list}/>
                <CommentBox 
                    list={list}
                    onAddComment={this.addComment.bind(this)}/>
            </div>
        )
    }
}

export default Comment