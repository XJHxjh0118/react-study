import React from 'react'

class LikeButton extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 0
        }
    }

    addLikes () {
        this.setState({
            count: ++this.state.count
        })
    }

    render () {
        return (
            <div className='likes-button-component'>
                <button
                    type='button'
                    onClick={this.addLikes.bind(this)}
                    className='btn button-outline-primary btn-lg'
                >
                    点赞 {this.state.count}
                </button>
            </div>
        )
    }
}

export default LikeButton