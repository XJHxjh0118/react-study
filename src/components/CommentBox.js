import React from 'react'

class CommentBox extends React.Component {
    constructor(props) {
        super(props)
        // this.state = {
        //     value: ''
        // }
    }
    handleChange (e) {
        this.setState({
            value: e.target.value
        })
    }
    handleSubmit (e) {
        e.preventDefault()
        alert(this.textInput.value)
    }

    render() {
        return (
            <form className='p-5' onSubmit={this.handleSubmit.bind(this)}>
                <div className='form-group'>
                    <label>留言内容</label>
                    <input
                        className='form-control'
                        placeholder='请输入留言内容'
                        // 非受控组件ref:将真实数据保存在dom中，不需要state支持
                        ref={(textInput) => {
                            this.textInput = textInput
                        }}
                        // onChange={this.handleChange.bind(this)}
                        // value={this.state.value}
                    />
                </div>
                <button
                    type='submit'
                    className='btn btn-primary'>
                    留言
                </button>
            </form>
        )
    }
}

export default CommentBox