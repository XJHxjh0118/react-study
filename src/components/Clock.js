import React from 'react'

class Clock extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            date: new Date()
        }
    }

    componentDidMount () {
        this.timeId = setInterval(() => {
            this.setState({
                date: new Date()
            })
        }, 1000)
    }

    componentDidUpdate(prevProps, prevState) {
        
    }

    componentWillUnmount () {
        clearInterval(this.timeId)
    }

    render () {
        return (
            <div className='clock jumbotron'>
                {this.state.date.toLocaleTimeString()}
            </div>
        )
    }
}

export default Clock