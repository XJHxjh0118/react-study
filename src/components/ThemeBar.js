import React from 'react'

import ThemeContext from '../theme-context'

const ThemeBar = () => {
    return (
        <ThemeContext.Consumer>
            {
                theme => {
                    let {classnames, color, bgColor} = theme
                    return (
                        <div
                            className='alert mt-5'
                            style={{
                                background: bgColor,
                                color
                            }}>
                            样式区域
                            <button className={classnames}>
                                样式按钮
                            </button>
                        </div>
                    )
                }
            }
        </ThemeContext.Consumer>
    )
}

export default ThemeBar